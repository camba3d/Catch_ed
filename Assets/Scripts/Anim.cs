﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;

public class Anim : MonoBehaviour
{

    bool OscilleUp;
    float maxHeight;
    float minHeight;
    float curHeight;
    float LerpFactor = 0.1f;

    private void Start()
    {
        maxHeight = transform.localScale.y;
        minHeight = maxHeight * 0.9f;
        curHeight = maxHeight;
    }

    public void Play(string action)
    {
        switch (action)
        {
            case "Bounce": Bounce(); break;
            default: break;
        }
    }


    private void Bounce()
    {
        if (curHeight > minHeight && !OscilleUp)
        {
            curHeight -= LerpFactor * Time.deltaTime;
        }
        else
        {
            OscilleUp = true;
            curHeight += LerpFactor * Time.deltaTime;
        }
        if (curHeight > maxHeight) { OscilleUp = false; }
        transform.localScale = new Vector3(
            transform.localScale.x,
            curHeight,
            transform.localScale.z
        );
    }
}
